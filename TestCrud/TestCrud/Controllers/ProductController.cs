﻿using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using TestCrud.Dto.Product;
using TestCrud.Validation.ProductValidator;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestCrud.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IMapper _mapper { get; }
        private IProductService _productService;
        public ProductController(IMapper mapper, IProductService productService, IValidator<Product> validator)
        {
            _mapper = mapper;
            _productService = productService;
        }
        // GET: api/<ProductController>
        [HttpGet]
        public  IActionResult GetAll()
        {
            try
            {
                var products =  _productService.GetAll();
                //return Ok(new { Data = products.Select(r => _mapper.Map<ProductDto>(r)) });
                return Ok(products);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product GetAll Exception: {0}", ex.Message, ex));
            }
        }
        /// <summary>
        /// Lấy danh sách công ty
        /// </summary>
        /// <param name="status, keyword, pageIndex, pageSize"></param>
        /// <returns></returns>
        [Route("List")]
        [HttpGet]
        public IActionResult List([FromQuery] ProductFilterDto filterDto)
        {
            /*var totalRecord = 0;*/
            try
            {
                var products = _productService.List(filterDto);

                /*var result = new
                {
                    Data = companies.Select(r => mapper.Map<CompanyDto>(r)),
                    Pagination = Pagination.Render(pageIndex, pageSize, totalRecord, Constants.PageStep, "{0}"),
                    TotalRecord = totalRecord
                };

                return Ok(result);*/
                return Ok(products);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product List Exception: {0}", ex.Message, ex));
            }
        }
        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            try
            {
                var item = _productService.Get(id);
                var product = _mapper.Map<ProductDto>(item);
                if (product == null || product.StatusId == 1)
                {
                    return NotFound();
                }
                return Ok(new { Data = _mapper.Map<ProductDto>(product) });
            }
            catch (Exception)
            {

                throw;
            }
        }

        // POST api/<ProductController>
        [HttpPost]
        public IActionResult Create(Product item)
        {
            try
            {
                var productValidator = new ProductValidator();
                var result = productValidator.Validate(item);
                if (!result.IsValid)
                {
                    var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
                    return BadRequest(errorMessages);
                }
                var message = string.Empty;
                _productService.Create(item);
                message = "Create successfull";
                return Ok(message);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product insert exception: {0}", ex.Message, ex));
            }
        }

        // PUT api/<ProductController>/5
        [HttpPut("{id}")]
        public IActionResult Update(long id, Product item)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            else
            {
                try
                {
                    var productValidator = new ProductValidator();
                    var result = productValidator.Validate(item);
                    if (!result.IsValid)
                    {
                        var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
                        return BadRequest(errorMessages);
                    }
                    var message = string.Empty;
                    item.Id = id;
                    _productService.Update(item);
                    message = "Update successfull!";
                    return Ok(message);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Product update exception: {0}", ex.Message, ex));
                }
            }
        }

        // DELETE api/<ProductController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                var message = string.Empty;
                _productService.Delete(id, 0);
                message = "Deleted successfull!";
                return Ok(message);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product delete exception: {0}", ex.Message, ex));
            }
        }
        /*//Hàm chuyển đổi đối tượng do FE đưa lên thành đối tượng nghiệp vụ trong hệ thống
        public IActionResult ConvertDTOToEntity(ProductDto productDto)
        {
            return Ok(_mapper.Map<Product>(productDto));
        }
        //Hàm chuyển đổi đối tượng Filter do FE đưa lên thành đối tượng Filter theo nghiệp vụ trong hệ thống
        public IActionResult ConvertFilterDTOToFilterEntity(FilterDto<ProductDto> productDto)
        {
            return Ok(_mapper.Map<Product>(productDto));
        }
        public int Count(FilterDto<ProductDto> filterDto)
        {
            var count = 0;
            //count = _productService.
            return count;
        }*/
    }
}
