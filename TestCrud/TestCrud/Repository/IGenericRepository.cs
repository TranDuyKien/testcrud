﻿namespace TestCrud
{
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Finds an entity by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        T Get(long id);

        /// <summary>
        /// Finds all entities.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
       IEnumerable<T> GetAll();

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        void Create(T item);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        void Update(T item);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        //void Delete(int id);
        void Delete(long id, long userId);
    }
}
