﻿using Microsoft.EntityFrameworkCore;

namespace TestCrud.Repository
{

    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }
        public async Task<int> CountAll()
        {
            var count = await _dbSet.CountAsync();
            return count;
        }
        public int Count()
        {
            return _dbSet.Count();
        }
        public IEnumerable<T> List()
        {
            return _dbSet.ToList();
        }
        public T Get(long id)
        {
            return _dbSet.Find(id)!;
        }
        public IEnumerable<T> GetAll()
        {
            return _dbSet.ToList();
        }

        void IGenericRepository<T>.Create(T item)
        {
            _dbSet.Add(item);
        }
        public void Delete(long id, long userId)
        {
            throw new NotImplementedException();
        }
        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
        /*public int CountAll(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = _context.Set<T>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.Count();
        }
        public IEnumerable<T> DynamicFind(Expression<Func<T, bool>> filter)
        {
            return _dbSet.Where(filter);
        }

        public IEnumerable<T> OrFilter(params Expression<Func<T, bool>>[] filters)
        {
            if (filters.Length == 0)
                return Enumerable.Empty<T>();

            var combinedFilter = filters[0];
            for (int i = 1; i < filters.Length; i++)
            {
                combinedFilter = Expression.Lambda<Func<T, bool>>(
                    Expression.OrElse(combinedFilter.Body, filters[i].Body),
                    combinedFilter.Parameters);
            }

            var filterExpression = Expression.Lambda<Func<T, bool>>(combinedFilter, filters[0].Parameters);
            return _dbSet.Where(filterExpression);
        }

        public IEnumerable<T> DynamicOrder(string property, bool isAscending)
        {
            var entityType = typeof(T);
            var parameter = Expression.Parameter(entityType, "x");
            var propertyExpression = Expression.Property(parameter, property);
            var lambdaExpression = Expression.Lambda<Func<T, dynamic>>(propertyExpression, parameter);

            return isAscending
                ? _dbSet.OrderBy(lambdaExpression)
                : _dbSet.OrderByDescending(lambdaExpression);
        }*/
    }

}
