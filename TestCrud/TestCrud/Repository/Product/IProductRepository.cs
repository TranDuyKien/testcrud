﻿using TestCrud.Dto.Product;

namespace TestCrud
{
    public interface IProductRepository: IGenericRepository<Product>
    {
        public IEnumerable<Product> List(ProductFilterDto filterDto/*, out int totalRecords*/);
    }
}
