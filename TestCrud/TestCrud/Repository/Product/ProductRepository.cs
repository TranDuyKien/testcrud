﻿using TestCrud.Dto.Product;
using TestCrud.Models;

namespace TestCrud.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(TestCrudContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Product> List(ProductFilterDto filterDto)
        {
            throw new NotImplementedException();
        }
    }
}
