﻿using FluentValidation;

namespace TestCrud.Validation.ProductValidator
{
    public class ProductValidator: AbstractValidator<Product>
    {
        public ProductValidator()
        {
            RuleFor(product => product.Name).NotEmpty().MaximumLength(128).WithMessage("Yêu cầu điền đầy đủ tên sản phẩm với tối đa 128 ký tự!");
            RuleFor(product => product.Category).NotEmpty().NotNull().MaximumLength(128).WithMessage("Yêu cầu điền thể loại sản phẩm với tối đa 128 ký tự!");
            RuleFor(product => product.UnitPrice).InclusiveBetween(0, decimal.MaxValue).WithMessage("Yêu cầu nhập đơn giá dương!");
            RuleFor(product => product.AvailableQuantity).InclusiveBetween(0, int.MaxValue).WithMessage("Yêu cầu nhập số lượng còn lại dương!");
        }
        /*public bool IsDecimal(string s)
        {
            Regex r = new Regex(@"^\d+\.?\d*$");
            return true;
        }*/
    }
}
