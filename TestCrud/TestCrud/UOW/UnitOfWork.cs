﻿using TestCrud.Models;
using TestCrud.Repository;
using TestCrud.UOW;

namespace TestCrud
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TestCrudContext _context;
        public IProductRepository Products { get; }
        public UnitOfWork(TestCrudContext context, IProductRepository catalogueRepository)
        {
            _context = context;
            Products = catalogueRepository;
        }
        public int Complete()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
