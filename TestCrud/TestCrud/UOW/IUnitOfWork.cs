﻿using TestCrud.Repository;

namespace TestCrud.UOW
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }
        int Complete();
    }
}
