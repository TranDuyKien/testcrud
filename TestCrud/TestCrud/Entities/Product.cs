﻿using System.ComponentModel.DataAnnotations;
using TestCrud.Entities;
using TestCrud.Validation.ProductValidator;

namespace TestCrud
{
    public class Product: BaseEntity
    {
        //[MaxLength(128)]
        public string Name { get; set; } = null!;
        //[MaxLength(128)]
        public string? Category { get; set; }
        /*[RegularExpression(@"^\d+(\.\d{2,2})$")]*/
        public decimal UnitPrice { get; set; }
        public int AvailableQuantity { get; set; }
    }
}
