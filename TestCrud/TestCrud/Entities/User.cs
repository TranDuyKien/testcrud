﻿using TestCrud.Entities;

namespace TestCrud
{
    public partial class User: BaseEntity
    {
        public long? RoleId { get; set; }
        public string? Username { get; set; }
        public string? Fullname { get; set; }
        public string? Mobile { get; set; }
        public string? Email { get; set; }
        public string? ImagePath { get; set; }
        public int? RecordStatus { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }

        public virtual Role? Role { get; set; }
    }
}
