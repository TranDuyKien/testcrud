﻿using System;
using System.Collections.Generic;
using TestCrud.Entities;

namespace TestCrud
{
    public partial class Menu: BaseEntity
    {
        public Menu()
        {
            MenuInRoleMappings = new HashSet<MenuInRoleMapping>();
            MenuInUserMappings = new HashSet<MenuInUserMapping>();
        }
        public long? ParentId { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? Url { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }

        public virtual ICollection<MenuInRoleMapping> MenuInRoleMappings { get; set; }
        public virtual ICollection<MenuInUserMapping> MenuInUserMappings { get; set; }
    }
}
