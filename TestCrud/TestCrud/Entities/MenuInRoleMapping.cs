﻿using System;
using System.Collections.Generic;
using TestCrud.Entities;

namespace TestCrud
{
    public partial class MenuInRoleMapping: BaseEntity
    {
        public long RoleId { get; set; }
        public long MenuId { get; set; }
        public virtual Menu Menu { get; set; } = null!;
        public virtual Role Role { get; set; } = null!;
    }
}
