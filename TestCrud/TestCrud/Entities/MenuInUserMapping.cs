﻿using System;
using System.Collections.Generic;
using TestCrud.Entities;

namespace TestCrud
{
    public partial class MenuInUserMapping: BaseEntity
    {
        public long UserId { get; set; }
        public long MenuId { get; set; }
        public virtual Menu Menu { get; set; } = null!;
    }
}
