﻿using Microsoft.EntityFrameworkCore;

namespace TestCrud.Models
{
    public partial class TestCrudContext : DbContext
    {
        public TestCrudContext()
        {
        }

        public TestCrudContext(DbContextOptions<TestCrudContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Menu> Menus { get; set; } = null!;
        public virtual DbSet<MenuInRoleMapping> MenuInRoleMappings { get; set; } = null!;
        public virtual DbSet<MenuInUserMapping> MenuInUserMappings { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<RoleDto> Roles { get; set; } = null!;
        public virtual DbSet<UserDto> Users { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=FISES-KIENDT8\\KIENTD8;Database=TestCrud;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu>(entity =>
            {
                entity.ToTable("Menu");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(64);

                entity.Property(e => e.DeleteAt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(128);

                entity.Property(e => e.ModifiedBy).HasMaxLength(64);

                entity.Property(e => e.Name).HasMaxLength(64);

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(256);
            });

            modelBuilder.Entity<MenuInRoleMapping>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.MenuId })
                    .HasName("PK_MenusInRoles");

                entity.ToTable("MenuInRoleMapping");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.MenuInRoleMappings)
                    .HasForeignKey(d => d.MenuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MenuInRol__MenuI__5535A963");

                /*entity.HasOne(d => d.Role)
                    .WithMany(p => p.MenuInRoleMappings)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MenuInRol__RoleI__5441852A");*/
            });

            modelBuilder.Entity<MenuInUserMapping>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.MenuId })
                    .HasName("PK_MenuInUser");

                entity.ToTable("MenuInUserMapping");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.MenuInUserMappings)
                    .HasForeignKey(d => d.MenuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MenuInUse__MenuI__5165187F");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Category)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.DeleteAt).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(128);

                entity.Property(e => e.DeleteAt).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(128);

                entity.Property(e => e.ModifiedBy).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(64);

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasMaxLength(64);

                entity.Property(e => e.DeleteAt).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(128);

                entity.Property(e => e.Fullname).HasMaxLength(64);

                entity.Property(e => e.ImagePath).HasMaxLength(512);

                entity.Property(e => e.Mobile).HasMaxLength(64);

                entity.Property(e => e.ModifiedBy).HasMaxLength(64);

                entity.Property(e => e.UpdateAt).HasColumnType("datetime");

                entity.Property(e => e.Username).HasMaxLength(64);

                /*entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__UserInfo__RoleId__00200768");*/
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
