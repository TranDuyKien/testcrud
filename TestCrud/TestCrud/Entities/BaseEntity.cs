﻿namespace TestCrud.Entities
{
    public class BaseEntity
    {
        public long Id { get; set; }
        public long StatusId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public DateTime? DeleteAt { get; set; }
        public bool? Used { get; set; }
    }
}
