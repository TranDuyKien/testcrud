﻿using TestCrud.Entities;

namespace TestCrud
{
    public partial class Role: BaseEntity
    {
        public Role()
        {
            MenuInRoleMappings = new HashSet<MenuInRoleMapping>();
            Users = new HashSet<UserDto>();
        }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }

        public virtual ICollection<MenuInRoleMapping> MenuInRoleMappings { get; set; }
        public virtual ICollection<UserDto> Users { get; set; }
    }
}
