﻿using AutoMapper;
using Microsoft.CodeAnalysis;
using TestCrud.Dto.Product;
using TestCrud.UOW;

namespace TestCrud.Service
{
    public class ProductService : IProductService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public ProductService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public void Create(Product item)
        {
            try
            {
                _unitOfWork.Products.Create(item);
                _unitOfWork.Complete();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product insert exception: {0}", ex.Message, ex));
            }
        }

        public void Delete(long id, long userId)
        {
            try
            {
                var product = _unitOfWork.Products.Get(id);
                if (product == null || product.StatusId == 1)
                {
                    throw new Exception(string.Format("Product update exception: {0}", "Product not exist or deleted!"));
                }
                else
                {
                    product.StatusId = 1;
                    _unitOfWork.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product update exception: {0}", ex.Message, ex));
            }
        }

        public Product Get(long id)
        {
            try
            {
                var product = _unitOfWork.Products.Get(id)!;
                return product;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Product> GetAll()
        {
            var listProduct = _unitOfWork.Products.GetAll().Where(e => e.StatusId == 0);
            return _mapper.Map<List<Product>>(listProduct);
        }

        public IEnumerable<Product> List(ProductFilterDto filterDto)
        {
            var listProductFilter = _unitOfWork.Products.List(filterDto).Where(e => e.Id == filterDto.Id 
            || e.Name == filterDto.Name 
            || e.Category == filterDto.Category 
            || e.UnitPrice == filterDto.UnitPrice
            || e.AvailableQuantity == filterDto.AvailableQuantity
            || e.StatusId == filterDto.StatusId
            || e.StatusId == filterDto.StatusId
            || e.Used == true).ToList();
            return listProductFilter;
        }

        public void Update(Product item)
        {
            try
            {
                var product = _unitOfWork.Products.Get(item.Id);
                if (product == null || product.StatusId == 1)
                {
                    throw new Exception(string.Format("Product update exception: {0}", "Product not exist or deleted!"));
                }
                else
                {
                    product.Name = item.Name;
                    product.Category = item.Category;
                    product.UnitPrice = item.UnitPrice;
                    product.AvailableQuantity = item.AvailableQuantity;
                    product.StatusId = item.StatusId;
                    _unitOfWork.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Product update exception: {0}", ex.Message, ex));
            }
        }
    }
}
