﻿using TestCrud.Dto.Product;
using TestCrud.Service;

namespace TestCrud
{
    public interface IProductService:IGenericService<Product>
    {
        public IEnumerable<Product> List(ProductFilterDto filterDto/*, out int totalRecords*/);

    }
}
