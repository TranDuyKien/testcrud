﻿using FluentValidation;
using TestCrud.Repository;
using TestCrud.Service;
using TestCrud.UOW;
using TestCrud.Validation.ProductValidator;

namespace TestCrud.Middleware
{
    public static class ServiceCollection
    {
        public static void RegisterIoCs(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<IValidator<Product>, ProductValidator>();
        }
    }
}
