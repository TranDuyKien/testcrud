﻿namespace TestCrud
{
    public partial class RoleDto
    {
        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public long? StatusId { get; set; }
        public bool? Used { get; set; }
    }
}
