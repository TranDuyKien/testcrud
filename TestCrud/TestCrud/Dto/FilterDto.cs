﻿namespace TestCrud
{
    public class FilterDto/*<T>*/
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int? TotalCount { get; set; }
        public long? Id { get; set; }
        //public List<T> Items { get; set; }

    }
}
