﻿namespace TestCrud
{
    public partial class UserDto
    {
        public long Id { get; set; }
        public long? RoleId { get; set; }
        public string? Username { get; set; }
        public string? Fullname { get; set; }
        public string? Mobile { get; set; }
        public string? Email { get; set; }
        public string? ImagePath { get; set; }
        public long? StatusId { get; set; }
        public bool? Used { get; set; }
    }
}
