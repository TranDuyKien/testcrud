﻿namespace TestCrud.Dto.Product
{
    public class ProductFilterDto :FilterDto
    {
        public string? Name { get; set; } = null!;
        public string? Category { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? AvailableQuantity { get; set; }
        public long? StatusId { get; set; }
        public bool? Used { get; set; }
    }
}
