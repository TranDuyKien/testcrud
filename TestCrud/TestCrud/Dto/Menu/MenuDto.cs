﻿namespace TestCrud
{
    public class MenuDto
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? Url { get; set; }
        public long? StatusId { get; set; }
        public bool? Used { get; set; }
    }
}
