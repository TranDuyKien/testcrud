﻿using AutoMapper;
using TestCrud.Dto.Product;

namespace TestCrud
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<Role, RoleDto>();
            CreateMap<User, RoleDto>();
            CreateMap<Product, ProductFilterDto>();
        }
    }
}
