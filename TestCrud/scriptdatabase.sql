USE [TestCrud]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 6/5/2023 5:32:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentId] [bigint] NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](128) NULL,
	[Url] [nvarchar](256) NULL,
	[StatusId] [bigint] NULL,
	[CreatedBy] [nvarchar](64) NULL,
	[ModifiedBy] [nvarchar](64) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[DeleteAt] [datetime] NULL,
	[Used] [bit] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuInRoleMapping]    Script Date: 6/5/2023 5:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuInRoleMapping](
	[RoleId] [bigint] NOT NULL,
	[MenuId] [bigint] NOT NULL,
	[StatusId] [bigint] NULL,
 CONSTRAINT [PK_MenusInRoles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuInUserMapping]    Script Date: 6/5/2023 5:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuInUserMapping](
	[UserId] [bigint] NOT NULL,
	[MenuId] [bigint] NOT NULL,
	[StatusId] [bigint] NULL,
 CONSTRAINT [PK_MenuInUser] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 6/5/2023 5:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Category] [varchar](128) NULL,
	[UnitPrice] [decimal](18, 0) NOT NULL,
	[AvailableQuantity] [int] NOT NULL,
	[StatusId] [bigint] NULL,
	[CreatedAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[DeleteAt] [datetime] NULL,
	[Used] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 6/5/2023 5:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](128) NULL,
	[StatusId] [bigint] NULL,
	[CreatedBy] [nvarchar](128) NULL,
	[ModifiedBy] [nvarchar](128) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[DeleteAt] [datetime] NULL,
	[Used] [bit] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/5/2023 5:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleId] [bigint] NULL,
	[Username] [nvarchar](64) NULL,
	[Fullname] [nvarchar](64) NULL,
	[Mobile] [nvarchar](64) NULL,
	[Email] [nvarchar](128) NULL,
	[ImagePath] [nvarchar](512) NULL,
	[StatusId] [bigint] NULL,
	[RecordStatus] [int] NULL,
	[CreatedBy] [nvarchar](64) NULL,
	[ModifiedBy] [nvarchar](64) NULL,
	[CreatedAt] [datetime] NULL,
	[UpdateAt] [datetime] NULL,
	[DeleteAt] [datetime] NULL,
	[Used] [bit] NULL,
 CONSTRAINT [PK_UserInfo_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MenuInRoleMapping]  WITH CHECK ADD  CONSTRAINT [FK__MenuInRol__MenuI__5535A963] FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menu] ([Id])
GO
ALTER TABLE [dbo].[MenuInRoleMapping] CHECK CONSTRAINT [FK__MenuInRol__MenuI__5535A963]
GO
ALTER TABLE [dbo].[MenuInRoleMapping]  WITH CHECK ADD  CONSTRAINT [FK__MenuInRol__RoleI__5441852A] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[MenuInRoleMapping] CHECK CONSTRAINT [FK__MenuInRol__RoleI__5441852A]
GO
ALTER TABLE [dbo].[MenuInUserMapping]  WITH CHECK ADD  CONSTRAINT [FK__MenuInUse__MenuI__5165187F] FOREIGN KEY([MenuId])
REFERENCES [dbo].[Menu] ([Id])
GO
ALTER TABLE [dbo].[MenuInUserMapping] CHECK CONSTRAINT [FK__MenuInUse__MenuI__5165187F]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK__UserInfo__RoleId__00200768] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK__UserInfo__RoleId__00200768]
GO
